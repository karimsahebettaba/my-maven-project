package MyPackage;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

//import org.junit.Test;

public class SamMethodTest {

	MyClassDemo myclassdemo;
	
	@BeforeEach
	void setUp(){
		myclassdemo = new MyClassDemo();
	}
	
	
	
	@Test
	@DisplayName("Simple multiplication test")
	void testmultiply() {
		assertEquals(20, myclassdemo.multiply(4,5), "Regular multiplication should work");
	}
	
	@RepeatedTest(5)
	@DisplayName("Ensure Handling with zerot")
	void testmultiplyWithZero() {
		assertEquals(0, myclassdemo.multiply(0,5), "Multiplication with zero should be zero");
		assertEquals(0, myclassdemo.multiply(5,0), "Multiplication with zero should be zero");
	}

}
